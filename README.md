# Minecraft Server Prober

Allows the Query/Probing of a Minecraft server about its online players, version, mods (if any) that would be
accessable from a normal Minecraft client.

## Usage

```text
minecraft_prober 0.1.0
HeapUnderflow <heapunderflow@outlook.com>
Query a minecraft server

USAGE:
    minecraft_prober [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -p, --port <PORT>        Override the port (25565 -> ':port' -> -p param)
    -s, --server <SERVER>    Set the server to query
    -t, --type <TYPE>        Set the assumed server type [default: forge]  [possible values: forge, vanilla]
```