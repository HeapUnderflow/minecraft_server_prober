use std::{
    io::{self, Read, Write},
    mem,
    net::TcpStream,
};

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ConnectionState {
    pub sent:     Vec<u8>,
    pub recieved: Vec<u8>,
}

impl Default for ConnectionState {
    fn default() -> Self {
        Self {
            sent:     Vec::new(),
            recieved: Vec::new(),
        }
    }
}

pub trait Connection {
    // This should return the state of e
    fn state_mut(&mut self) -> &mut ConnectionState;
    fn state(&self) -> &ConnectionState;

    fn read(&mut self, length: usize) -> io::Result<Vec<u8>> {
        let current_state = self.state_mut();

        if length > current_state.recieved.len() {
            return Err(io::Error::new(
                io::ErrorKind::UnexpectedEof,
                String::from("Unable to read bytes: EOF"),
            ));
        }

        let newvec = current_state.recieved.split_off(length);
        let result = mem::replace(&mut current_state.recieved, newvec);
        Ok(result)
    }

    fn write(&mut self, data: &[u8]) -> io::Result<usize> {
        self.state_mut().sent.extend_from_slice(data);
        Ok(data.len())
    }

    fn receive(&mut self, data: &[u8]) -> io::Result<usize> {
        self.state_mut().recieved.extend_from_slice(data);
        Ok(data.len())
    }

    fn remaining(&self) -> usize { self.state().recieved.len() }

    fn flush(&mut self) -> Vec<u8> { mem::replace(&mut self.state_mut().sent, Vec::new()) }

    fn read_varint(&mut self) -> io::Result<i32> {
        let mut num_read = 0;
        let mut result: u32 = 0;

        'read_loop: loop {
            let read: u8 = self.read(1)?[0];
            let val: u32 = u32::from(read & 0b0111_1111);
            result |= val << (7 * num_read);

            num_read += 1;

            if num_read > 5 {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    String::from("Varint to big !"),
                ));
            }

            if (read & 0b1000_0000) == 0 {
                break 'read_loop;
            }
        }
        Ok(result as i32)
    }

    fn write_varint(&mut self, varint: i32) -> io::Result<usize> {
        let mut num_written = 0;
        let mut data = Vec::new();

        let mut varint = varint as u32;

        'write_loop: loop {
            let mut tmp = u8::to_be((varint & 0b0111_1111) as u8);
            varint >>= 7;
            if varint != 0 {
                tmp |= 0b1000_0000;
            }

            data.push(tmp);

            num_written += 1;

            if num_written > 5 {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    String::from("numer to big to be store in varint to big !"),
                ));
            }

            if varint == 0 {
                break 'write_loop;
            }
        }

        self.write(&data)
    }

    fn read_utf8(&mut self) -> io::Result<String> {
        let string_length = self.read_varint()?;
        match String::from_utf8(self.read(string_length as usize)?) {
            Ok(o) => Ok(o),
            Err(e) => Err(io::Error::new(io::ErrorKind::InvalidData, e)),
        }
    }

    fn write_utf8(&mut self, data: String) -> io::Result<usize> {
        let bt = data.into_bytes();
        self.write_varint(bt.len() as i32)?;
        self.write(&bt)
    }

    // TODO: Read ASCII (https://github.com/Dinnerbone/mcstatus/blob/master/mcstatus/protocol/connection.py#L68)

    fn read_short(&mut self) -> io::Result<i16> {
        let mut data: [u8; 2] = [0x00; 2];
        data.copy_from_slice(&self.read(2)?);
        Ok(i16::from_be_bytes(data))
    }

    fn write_short(&mut self, value: i16) -> io::Result<usize> {
        let packed = value.to_be_bytes();
        self.write(&packed)
    }

    fn read_ushort(&mut self) -> io::Result<u16> {
        let mut data: [u8; 2] = [0x00; 2];
        data.copy_from_slice(&self.read(2)?);
        Ok(u16::from_be_bytes(data))
    }

    fn write_ushort(&mut self, value: u16) -> io::Result<usize> {
        let packed = value.to_be_bytes();
        self.write(&packed)
    }

    fn read_long(&mut self) -> io::Result<i64> {
        let mut data: [u8; 8] = [0x00; 8];
        data.copy_from_slice(&self.read(8)?);
        Ok(i64::from_be_bytes(data))
    }

    fn write_long(&mut self, value: i64) -> io::Result<usize> {
        let packed = value.to_be_bytes();
        self.write(&packed)
    }

    fn read_ulong(&mut self) -> io::Result<u64> {
        let mut data: [u8; 8] = [0x00; 8];
        data.copy_from_slice(&self.read(8)?);
        Ok(u64::from_be(u64::from_be_bytes(data)))
    }

    fn write_ulong(&mut self, value: u64) -> io::Result<usize> {
        let packed = value.to_be_bytes();
        self.write(&packed)
    }

    fn read_buffer(&mut self) -> io::Result<DefaultConnection> {
        let length = self.read_varint()?;

        let mut res = DefaultConnection::new();
        res.receive(&self.read(length as usize)?)?;
        Ok(res)
    }

    fn write_buffer<C>(&mut self, mut buffer: C) -> io::Result<usize>
    where
        C: Connection,
    {
        let data = buffer.flush();
        self.write_varint(data.len() as i32)?;
        self.write(&data)
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct DefaultConnection {
    state: ConnectionState,
}

impl DefaultConnection {
    pub fn new() -> Self {
        Self {
            state: ConnectionState::default(),
        }
    }
}

impl Default for DefaultConnection {
    fn default() -> Self {
        Self {
            state: ConnectionState::default(),
        }
    }
}

impl Connection for DefaultConnection {
    fn state_mut(&mut self) -> &mut ConnectionState { &mut self.state }

    fn state(&self) -> &ConnectionState { &self.state }
}

#[derive(Debug)]
pub struct TCPSocketConnection {
    state:  ConnectionState,
    socket: TcpStream,
}

impl TCPSocketConnection {
    pub fn new(addr: String) -> io::Result<Self> {
        let tcp = TcpStream::connect(addr)?;
        Ok(Self {
            state:  ConnectionState::default(),
            socket: tcp,
        })
    }
}

impl Connection for TCPSocketConnection {
    fn state(&self) -> &ConnectionState { &self.state }

    fn state_mut(&mut self) -> &mut ConnectionState { &mut self.state }

    fn flush(&mut self) -> Vec<u8> {
        panic!("TCPSocketConnection does not support 'flush'");
    }

    fn receive(&mut self, _: &[u8]) -> io::Result<usize> {
        panic!("TCPSocketConnection does not support 'recieve'");
    }

    fn remaining(&self) -> usize {
        panic!("TCPSocketConnection does not support 'remaining'");
    }

    fn read(&mut self, _length: usize) -> io::Result<Vec<u8>> {
        let mut result = Vec::new();
        while result.len() < _length {
            let mut new = vec![0u8; _length - result.len()];

            self.socket.read_exact(&mut new)?;

            if new.is_empty() {
                return Err(io::Error::new(
                    io::ErrorKind::UnexpectedEof,
                    String::from("Server did not respond with any information!"),
                ));
            }
            result.extend_from_slice(&new);
        }

        Ok(result)
    }

    fn write(&mut self, data: &[u8]) -> io::Result<usize> { self.socket.write(data) }
}

#[cfg(test)]
mod tests {
    use super::{Connection, DefaultConnection};
    #[test]
    fn varint_write_test() {
        let parts = vec![
            (0i32, vec![0x00u8]),
            (1i32, vec![0x01u8]),
            (2i32, vec![0x02u8]),
            (127i32, vec![0x7Fu8]),
            (128i32, vec![0x80u8, 0x01u8]),
            (255i32, vec![0xFFu8, 0x01u8]),
            (2147483647i32, vec![0xFFu8, 0xFFu8, 0xFFu8, 0xFFu8, 0x07u8]),
            (-1, vec![0xFFu8, 0xFFu8, 0xFFu8, 0xFFu8, 0x0Fu8]),
            (-2147483648, vec![0x80u8, 0x80u8, 0x80u8, 0x80u8, 0x08u8]),
        ];

        for test in parts {
            let mut df = DefaultConnection::new();
            df.write_varint(test.0).unwrap();
            assert_eq!(test.1, df.state().sent);
        }
    }

    #[test]
    fn varint_read_test() {
        let parts = vec![
            (0i32, vec![0x00u8]),
            (1i32, vec![0x01u8]),
            (2i32, vec![0x02u8]),
            (127i32, vec![0x7Fu8]),
            (128i32, vec![0x80u8, 0x01u8]),
            (255i32, vec![0xFFu8, 0x01u8]),
            (2147483647i32, vec![0xFFu8, 0xFFu8, 0xFFu8, 0xFFu8, 0x07u8]),
            (-1, vec![0xFFu8, 0xFFu8, 0xFFu8, 0xFFu8, 0x0Fu8]),
            (-2147483648, vec![0x80u8, 0x80u8, 0x80u8, 0x80u8, 0x08u8]),
        ];

        for test in parts {
            let mut df = DefaultConnection::new();
            // Prepping state
            df.state_mut().recieved = test.1;

            let i = df.read_varint();
            let r = match i {
                Ok(o) => o,
                Err(e) => panic!(format!("{:?}", e)),
            };
            assert_eq!(test.0, r);
        }
    }
}
