use super::protocol::connection::{Connection, DefaultConnection, TCPSocketConnection};
use rand::random;
use serde_derive::{Deserialize, Serialize};
use serde_json;
use std::{io, time::Instant};

#[derive(Debug)]
pub struct ServerPinger {
    connection: TCPSocketConnection,
    version:    i32,
    host:       String,
    port:       u16,
    token:      i64,
}

impl ServerPinger {
    pub fn new<H, P, V, T>(
        connection: TCPSocketConnection,
        host: H,
        port: P,
        version: V,
        ping_token: T,
    ) -> ServerPinger
    where
        H: Into<Option<String>>,
        P: Into<Option<u16>>,
        V: Into<Option<i32>>,
        T: Into<Option<i64>>,
    {
        ServerPinger {
            connection,
            host: host.into().unwrap_or_else(|| String::from("")),
            port: port.into().unwrap_or(0),
            version: version.into().unwrap_or(47),
            token: ping_token.into().unwrap_or_else(random::<i64>),
        }
    }

    pub fn handshake(&mut self) -> io::Result<usize> {
        let mut packet = DefaultConnection::new();
        packet.write_varint(0)?;
        packet.write_varint(self.version)?;
        packet.write_utf8(self.host.clone())?;
        packet.write_ushort(self.port)?;
        packet.write_varint(1)?;

        self.connection.write_buffer(packet)
    }

    pub fn handshake_fml(&mut self) -> io::Result<usize> {
        let mut packet = DefaultConnection::new();
        packet.write_varint(0)?;
        packet.write_varint(self.version)?;
        packet.write_utf8(format!("{}\0FML\0", self.host))?;
        packet.write_ushort(self.port)?;
        packet.write_varint(1)?;

        self.connection.write_buffer(packet)
    }

    pub fn read_status(&mut self) -> io::Result<PingResponse> {
        let mut request = DefaultConnection::new();
        request.write_varint(0)?;
        self.connection.write_buffer(request)?;

        let mut response = self.connection.read_buffer()?;
        if response.read_varint()? != 0 {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                String::from("Recieved invalid response packet."),
            ));
        }

        match serde_json::from_str::<PingResponse>(&response.read_utf8()?) {
            Ok(o) => Ok(o),
            Err(e) => Err(io::Error::new(io::ErrorKind::InvalidData, e)),
        }
    }

    pub fn test_ping(&mut self) -> io::Result<f64> {
        let mut request = DefaultConnection::new();
        request.write_varint(1)?;
        request.write_long(self.token)?;
        let sent = Instant::now();

        self.connection.write_buffer(request)?;

        let mut response = self.connection.read_buffer()?;
        let delta = sent.elapsed();

        if response.read_varint()? != 1 {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                String::from("Recieved invalid ping response packet."),
            ));
        }

        let rtoken = response.read_long()?;
        if rtoken != self.token {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                format!(
                    "Recieved mangled ping response packet (expected {}, recieved {})",
                    self.token, rtoken
                ),
            ));
        }

        Ok((delta.as_secs() as f64 * 1000.0) + f64::from(delta.subsec_millis()))
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)] // TODO: Implement Defaults
pub struct PingResponse {
    pub players:     Players,
    pub version:     Version,
    pub description: serde_json::Value,
    pub favicon:     Option<String>,
    pub modinfo:     Option<ModInfo>,
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq)]
pub struct ModInfo {
    #[serde(rename = "type")]
    pub mods_type: String,
    #[serde(rename = "modList")]
    pub modlist: Vec<Mod>,
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq)]
pub struct Mod {
    pub modid:   String,
    pub version: String,
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq)]
pub struct Players {
    pub online: i32,
    pub max:    i32,
    pub sample: Option<Vec<Player>>,
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq)]
pub struct Player {
    pub name: String,
    #[serde(rename = "id")]
    pub uuid: String,
}

#[derive(Debug, Clone, Serialize, Deserialize, Eq, PartialEq)]
pub struct Version {
    pub name:     String,
    pub protocol: i32,
}

// TODO: Tests
