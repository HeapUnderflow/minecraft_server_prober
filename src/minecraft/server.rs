use super::{
    pinger::{PingResponse, ServerPinger},
    protocol::connection::TCPSocketConnection,
};
use serde_derive::{Deserialize, Serialize};
use std::io;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct MinecraftServer {
    host: String,
    port: u16,
}

impl MinecraftServer {
    pub fn new<P>(host: String, port: P) -> MinecraftServer
    where
        P: Into<Option<u16>>,
    {
        MinecraftServer {
            host,
            port: port.into().unwrap_or(25565),
        }
    }

    pub fn ping(&mut self) -> io::Result<f64> {
        let conn = TCPSocketConnection::new(format!("{}:{}", self.host, self.port))?;
        let mut pinger = ServerPinger::new(conn, self.host.clone(), self.port, 5, None);
        pinger.handshake_fml()?;
        pinger.test_ping()
    }

    pub fn status_fml(&mut self) -> io::Result<StatusResult> {
        let conn = TCPSocketConnection::new(format!("{}:{}", self.host, self.port))?;
        let mut pinger = ServerPinger::new(conn, self.host.clone(), self.port, 5, None);
        pinger.handshake_fml()?;
        Ok(StatusResult {
            data:    pinger.read_status()?,
            latency: pinger.test_ping()?,
        })
    }

    pub fn status(&mut self) -> io::Result<StatusResult> {
        let conn = TCPSocketConnection::new(format!("{}:{}", self.host, self.port))?;
        let mut pinger = ServerPinger::new(conn, self.host.clone(), self.port, 5, None);
        pinger.handshake()?;
        Ok(StatusResult {
            data:    pinger.read_status()?,
            latency: pinger.test_ping()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)] // TODO: Implement Defaults
pub struct StatusResult {
    pub latency: f64,
    pub data:    PingResponse,
}

// TODO: Tests
