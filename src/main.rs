pub mod minecraft;
use crate::minecraft::server::MinecraftServer;
use serde_json::Value as JsonValue;

use clap::clap_app;

fn main() {
    let matches = clap_app!(minecraft_prober =>
        (version: env!("CARGO_PKG_VERSION"))
        (author: env!("CARGO_PKG_AUTHORS"))
        (about: "Query a minecraft server")
        (@arg SERVER: -s --server +takes_value "Set the server to query")
        (@arg PORT: -p --port +takes_value "Override the port (25565 -> ':port' -> -p param)")
        (@arg TYPE: -t --type +takes_value possible_value[forge vanilla] default_value[forge] "Set the assumed server type")
    ).get_matches();

    let address = matches.value_of("SERVER").expect("No server url set");
    let mut host;
    let mut port = None;

    if address.contains(':') {
        let parts = address.split(':').collect::<Vec<_>>();
        if parts.len() > 2 {
            host = address.to_string();
        } else {
            host = parts[0].to_string();
            port = Some(parts[1].parse::<u16>().unwrap_or(25565));
        }
    } else {
        host = address.to_string();
    }

    if port == None {
        port = Some(25565);
    };

    if let Some(nport) = matches.value_of("PORT") {
        if let Ok(pp) = nport.parse() {
            port = Some(pp);
        }
    }

    let mut mcs = MinecraftServer::new(host, port.expect(concat!("PORT: LogicError ", line!())));
    let status = match matches.value_of("TYPE") {
        Some("forge") => mcs.status_fml(),
        Some("vanilla") => mcs.status(),
        _ => panic!("Invalid option"),
    }
    .expect("Failed to request status");

    println!(
        "Version: {} (Protocol: {})",
        status.data.version.name, status.data.version.protocol
    );
    match status.data.description {
        JsonValue::String(s) => println!("Description: {}", s),
        o => println!("Description: {:?}", o),
    }
    println!("Latency: {}ms", status.latency);
    println!(
        "Players ({}/{}):",
        status.data.players.online, status.data.players.max
    );
    if let Some(sample) = status.data.players.sample {
        for p in sample {
            println!("| {}: {}", p.uuid, p.name);
        }
    }
    if let Some(modinfo) = status.data.modinfo {
        println!("ServerType: {}", modinfo.mods_type);
        println!("Mods ({})", modinfo.modlist.len());
        for md in modinfo.modlist {
            println!("| {} [{}]", md.modid, md.version);
        }
    } else {
        println!("ServerType: Vanilla (no fml)");
    }
}
